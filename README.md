## Objectives

1. Students will apply what they've learned about JavaScript, p5 and A-Frame to creating final project
2. Students will solidify the concepts they've learned
3. Students will leave with a tangible example of their work

## Resources

* [How to Pair Program](https://www.youtube.com/watch?v=YhV4TaZaB84)
* [Pair Programming Tips](https://www.tutorialspoint.com/extreme_programming/extreme_programming_pair_programming.htm)
